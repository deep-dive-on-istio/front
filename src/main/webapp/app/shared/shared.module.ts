import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FrontSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [FrontSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [FrontSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FrontSharedModule {
  static forRoot() {
    return {
      ngModule: FrontSharedModule
    };
  }
}
