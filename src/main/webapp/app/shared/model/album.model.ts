export interface IAlbum {
  id?: number;
  name?: string;
  descripcion?: string;
}

export class Album implements IAlbum {
  constructor(public id?: number, public name?: string, public descripcion?: string) {}
}
