export interface ITrack {
  id?: number;
  name?: string;
  artists?: string;
  star?: number;
}

export class Track implements ITrack {
  constructor(public id?: number, public name?: string, public artists?: string, public star?: number) {}
}
