import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'track',
        loadChildren: () => import('./track/track.module').then(m => m.FrontTrackModule)
      },
      {
        path: 'album',
        loadChildren: () => import('./album/album.module').then(m => m.FrontAlbumModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
    NgbModule
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FrontEntityModule {}
