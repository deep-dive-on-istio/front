import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { FrontSharedModule } from 'app/shared';
import {
  TrackComponent,
  TrackDetailComponent,
  TrackUpdateComponent,
  TrackDeletePopupComponent,
  TrackDeleteDialogComponent,
  trackRoute,
  trackPopupRoute
} from './';

const ENTITY_STATES = [...trackRoute, ...trackPopupRoute];

@NgModule({
  imports: [FrontSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TrackComponent, TrackDetailComponent, TrackUpdateComponent, TrackDeleteDialogComponent, TrackDeletePopupComponent],
  entryComponents: [TrackComponent, TrackUpdateComponent, TrackDeleteDialogComponent, TrackDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FrontTrackModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
