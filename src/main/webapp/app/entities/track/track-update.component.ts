import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITrack, Track } from 'app/shared/model/track.model';
import { TrackService } from './track.service';

@Component({
  selector: 'jhi-track-update',
  templateUrl: './track-update.component.html'
})
export class TrackUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    artists: []
  });

  constructor(protected trackService: TrackService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ track }) => {
      this.updateForm(track);
    });
  }

  updateForm(track: ITrack) {
    this.editForm.patchValue({
      id: track.id,
      name: track.name,
      artists: track.artists
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const track = this.createFromForm();
    if (track.id !== undefined) {
      this.subscribeToSaveResponse(this.trackService.update(track));
    } else {
      this.subscribeToSaveResponse(this.trackService.create(track));
    }
  }

  private createFromForm(): ITrack {
    return {
      ...new Track(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      artists: this.editForm.get(['artists']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrack>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
