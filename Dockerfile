FROM nginx:latest
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY  build/resources/main/static /usr/share/nginx/html
